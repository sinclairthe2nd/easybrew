import csv
import os
import time

def brew():
    os.system('cls' if os.name == 'nt' else 'clear')
    myDataR =[]
    name = input("Recipe File?:")
    name +=".csv"
    with open(name) as File:
        reader = csv.reader(File, delimiter=',', quotechar=',',
                        quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            myDataR.extend(row)
    index = 0
    dic = {"+":"This makes: [L]","!":"Add Malt [Name,Kg]", "§":"Mash at [°C]", "$":"Mash for[min]","%":"Sparge with[L]","&":"Sparge at [°C]","/":"Boil for [min]","(":"Add Bittering Hops at beginning of Boil[g]","=":"Add Additional Hops","*":"Use","'":"Add Sugar before bottling [g]","~":"Dry Hop with [g]"}
    myDataR = [dic.get(n, n) for n in myDataR]    
    count = 11
    for x in range (0,count):
        print(myDataR[index])
        index += 1
    print("Start Mashing at [°C]")
    print(myDataR[12])
    noneed = input("Ready? ")
    inp = int(myDataR[14])
    cinp = inp
    print("Mash for Another [min]: ")
    for a in range(inp):
        print(cinp)
        cinp -= 1
        time.sleep(0.5)
    print("Mash Out")
    print(myDataR[15])
    print(myDataR[16])
    print(myDataR[17])
    print(myDataR[18]) 
    print(myDataR[19])
    print(myDataR[21])
    print(myDataR[22])
    print(myDataR[23])
    inl = int(myDataR[20])
    cinl =inl
    noneed = input("Ready? ")
    print("Boil for Another [min]: ")
    for a in range(inl):
        print(cinl)
        if cinl == int(myDataR[27]):
            print(myDataR[24],myDataR[25],myDataR[26],"[g]")
        elif cinl == int(myDataR[30]):
            print(myDataR[24],myDataR[28],myDataR[29],"[g]")
        elif cinl == int(myDataR[32]):
            print(myDataR[24],myDataR[31],myDataR[32],"[g]")
        elif cinl == int(myDataR[36]):
            print(myDataR[24],myDataR[34],myDataR[35],"[g]")
        cinl -= 1
        time.sleep(0.5)
    print(myDataR[37])
    print(myDataR[38])
    print(myDataR[39])
    print(myDataR[40])
    print(myDataR[41])
    print(myDataR[42])
    print(myDataR[43])
     
    return

def show():
    os.system('cls' if os.name == 'nt' else 'clear')
    myDataR =[]
    name = input("Recipe File?:")
    name +=".csv"
    with open(name) as File:
        reader = csv.reader(File, delimiter=',', quotechar=',',
                        quoting=csv.QUOTE_MINIMAL)
        for row in reader:
            myDataR.extend(row)
    index = 0
    dic = {"+":"This makes: [L]","!":"Add Malt", "§":"Mash at [°C]", "$":"Mash for[min]","%":"Sparge with[L]","&":"Sparge at [°C]","/":"Boil for [min]","(":"Add Bittering Hops","=":"Add Additional Hops","*":"Use","'":"Add Sugar before bottling [g]","~":"Dry Hop with [g]"}
    myDataR = [dic.get(n, n) for n in myDataR]    
    count = (len(myDataR))
    for x in range (0,count):
        print(myDataR[index])
        index += 1
         
    return

def write():
    os.system('cls' if os.name == 'nt' else 'clear')
    name = input("Name :")
    name += ".csv"
    volume = input("How many Units of Volume does this Recipe make?:" )
    Fname1, Fweight1 = [(x) for x in input("Fermentable 1 Name,Weight: ").split()]
    Fname2, Fweight2 = [(x) for x in input("Fermentable 2 Name,Weight: ").split()]       
    Fname3, Fweight3 = [(x) for x in input("Fermentable 3 Name,Weight: ").split()]
    Fname4, Fweight4 = [(x) for x in input("Fermentable 4 Name,Weight: ").split()]

    mtemp = input("Mashing Temperature: ")
    mtime = input("Mashing Time: ")
    svol = input("Sparging Volume: ")
    stemp = input("Sparging Temp: ")
    btime = input("Boiling Time: ")
    bhopss, bhopsw = [(x) for x in input("Bittering Hops Name,Weight: ").split()]
    Hname1, Hweight1 = [(x) for x in input("Additional Hops 1,Weight: ").split()]
    hopt1 =input("add how long before end of boil?: ")
    Hname2, Hweight2 = [(x) for x in input("Additional Hops 2,Weight: ").split()]
    hopt2 =input("add how long before end of boil?: ")
    Hname3, Hweight3 = [(x) for x in input("Additional Hops 3,Weight: ").split()]
    hopt3 =input("add how long before end of boil?: ")
    Hname4, Hweight4 = [(x) for x in input("Additional Hops 4,Weight: ").split()]
    hopt4 =input("add how long before end of boil?: ")
    sugar =input("How much Sugar for Carbonation?: ")
    yeast =input("Recommended Yeast: ")
    dryhop =input("dry hop with?")
    dryhopw =input("Weight?") 
    myData=[["+"],[volume],["!"],[Fname1],[Fweight1],[Fname2],[Fweight2],[Fname3],[Fweight3],[Fname4],[Fweight4],["§"],[mtemp],["$"],[mtime],["%"],[svol],["&"],[stemp],["/"],[btime],["("],[bhopss],[bhopsw],["="],[Hname1],[Hweight1],[hopt1],[Hname2],[Hweight2],[hopt2],[Hname3],[Hweight3],[hopt3],[Hname4],[Hweight4],[hopt4],["*"],[yeast],["'"],[sugar],["~"],[dryhop],[dryhopw]]

    print(myData)
    myFile = open(name, 'w')
    with open(name, 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(myData)
     

    return



os.system('cls' if os.name == 'nt' else 'clear')

print("This is EasyBrew")

print("1 Brew Recipe")
print("2 Show Recipe")
print("3 Write New Recipe")
print("4 Edit Recipe")
selector = int(input("Please choose what you want to do!"))

if(selector == 1):
    brew()
elif(selector == 2):
    show()
elif(selector == 3):
    write()

